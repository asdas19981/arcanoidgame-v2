package com.ads.arcanoid;

import com.ads.arcanoid.Controller.ControlType;
import com.ads.arcanoid.Screens.GameScreen;
import com.ads.arcanoid.Screens.HelpScreen;
import com.ads.arcanoid.Screens.HighscoreScreen;
import com.ads.arcanoid.Screens.MainMenuScreen;
import com.ads.arcanoid.Screens.SettingsMenuScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;

public class ArcanoidGame extends Game {
    private static ArcanoidGame ourInstance = new ArcanoidGame();

    private ArcanoidGame() {}
    public static ArcanoidGame getInstance() {
        return ourInstance;
    }

    private HashMap<String, TextureRegion> textureRegions;
    SpriteBatch batch;
    GameScreen gameScreen;
    HelpScreen helpScreen;
    HighscoreScreen highscoreScreen;
    MainMenuScreen mainMenuScreen;
    SettingsMenuScreen settingsMenuScreen;
    private float ppuX;
    private float ppuY;
    private ControlType controlType;

    @Override
    public void create() {
        //Размеры моего мира 920 на 540
        ppuX = (float)Gdx.graphics.getWidth()/920f;
        ppuX = (float)Gdx.graphics.getHeight()/540f;
        controlType = ControlType.TOUCH;
        loadGraphics();
        batch = new SpriteBatch();
        gameScreen = new GameScreen(batch, textureRegions);
        helpScreen = new HelpScreen(batch, textureRegions);
        highscoreScreen = new HighscoreScreen(batch, textureRegions);
        mainMenuScreen = new MainMenuScreen(batch, textureRegions);
        settingsMenuScreen = new SettingsMenuScreen(batch, textureRegions);
        moveToMainMenu();
    }

    private void loadGraphics() {
        Texture bricks = new Texture("android/assets/breakout_custom.png");
        textureRegions = new HashMap<String, TextureRegion>();
        textureRegions.put("blank background", new TextureRegion(bricks, 138, 48, 1, 1));
        textureRegions.put("blue-brick", new TextureRegion(bricks, 0, 0, 32, 16));
        textureRegions.put("blue-small-stick", new TextureRegion(bricks, 0, 64, 32, 16));
        textureRegions.put("blue-normal-stick", new TextureRegion(bricks, 32, 64, 64, 16));
        textureRegions.put("blue-long-stick", new TextureRegion(bricks, 96, 64, 96, 16));
        textureRegions.put("blue-longest-stick", new TextureRegion(bricks, 0, 80, 128, 16));
        textureRegions.put("blue-small-ball", new TextureRegion(bricks, 96, 48, 8, 8));
        textureRegions.put("blue-big-ball", new TextureRegion(bricks, 96, 48, 8, 8));

    }

    public void moveToMainMenu() {
        setScreen(mainMenuScreen);
    }

    public void moveToSettingsMenu() {
        setScreen(settingsMenuScreen);
    }

    public void moveToHelp() {
        setScreen(helpScreen);
    }

    public void moveToHighscore() {
        setScreen(highscoreScreen);
    }

    public void moveToGame() {
        setScreen(gameScreen);
    }

    public float getPpuX() {
        return ppuX;
    }

    public void setPpuX(float ppuX) {
        this.ppuX = ppuX;
    }

    public float getPpuY() {
        return ppuY;
    }

    public void setPpuY(float ppuY) {
        this.ppuY = ppuY;
    }

    public ControlType getControlType() {
        return controlType;
    }
    public HashMap<String, TextureRegion> getTextureRegions(){return textureRegions;}
}
