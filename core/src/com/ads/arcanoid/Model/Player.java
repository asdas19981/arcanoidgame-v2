package com.ads.arcanoid.Model;

import com.ads.arcanoid.Controller.Direction;
import com.ads.arcanoid.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;


/**
 * Created by ga_nesterchuk on 27.01.2016.
 */
public class Player extends ImageActor {

    private Direction direction;
    private float step;

    public Player(TextureRegion img, float x, float y, float width, float height, float step) {
        super(img, x, y, width, height);
        direction = Direction.NONE;
        this.step = step;
    }

    public Player(Texture img, float x, float y, float step) {
        super(img, x, y);
        direction = Direction.NONE;
        this.step = step;
    }

    public Player(Texture img, float x, float y, float width, float height, float step) {
        super(img, x, y, width, height);
        direction = Direction.NONE;
        this.step = step;
    }

    public Player(TextureRegion img, float x, float y, float step) {
        super(img, x, y);
        direction = Direction.NONE;
        this.step = step;
    }

    public void setDirection(Direction direction){
        this.direction = direction;
    }
    public void act(float delta) {
        switch (direction) {
            case LEFT:
                moveBy(-step*delta, 0);
                if (getX() < 0)
                    setX(0);
                break;
            case RIGHT:
                moveBy(step*delta, 0);
                if (getX() + getWidth() > 920)
                    setX(920 - getWidth());
                break;
        }
    }
}
